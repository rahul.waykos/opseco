//To GET the RESPONSE from the API and load it. 
(function () {
    $.ajax({
    url: "https://opseco-website.cdn.prismic.io/api/v1/documents/search?ref=Xvzo_hAAACkAqnza",
    // data: data,
    // dataType: json,
    success: function(data){
        // console.log(data);
        var page = data.results[0].data.opseco;
        // console.log(page.name.value[0].text);
        // console.log(page.title1.value[0].text);
        // console.log(page.image.value.main.url);

        //LANDING PAGE TITLE AND DISCRIPTION
        result = page.landing_text.value[0].text;
        landing_desc1 = page.landing_description.value[0].text;
        console.log(landing_desc1);
        // OUR SERVICES SECTION DATA
        service_one_title = page.service_one_title.value[0].text;
        service_one_desc = page.service_one_desc.value[0].text;
        service_one_image = page.service_one_image.value.main.url;

        service_two_title = page.service_two_title.value[0].text;
        service_two_desc = page.service_two_desc.value[0].text;
        service_two_image = page.service_two_image.value.main.url;

        service_three_title = page.service_three_title.value[0].text;
        service_three_desc = page.service_three_desc.value[0].text;
        service_three_image = page.service_three_image.value.main.url;

        service_four_title = page.service_four_title.value[0].text;
        service_four_desc = page.service_four_desc.value[0].text;
        service_four_image = page.service_four_image.value.main.url;

        //ABOUT US DATA
        about_us_desc = page.about_us_desc.value[0].text;

        //CAREERS DATA
        job_title = page.job_title.value[0].text;
        job_desc = page.job_description.value[0].text;
        job_skill_1 = page.skills_1.value[0].text;
        job_skill_2 = page.skills_2.value[0].text;
        job_skill_3 = page.skills_3.value[0].text;
        job_skill_4 = page.skill_4.value[0].text;
        optional_cont = page.optional_content.value[0].text;
        job_date = page.job_date.value;
        // product_image = page.image.value.main.url;
        loadResult(result);
    },
    });
})();

//To convert the data into the string and display in html tag using html tag ID's
function loadResult(result){

    //SHOWING DATA IN LANDING PAGE SECTION
    $("#landing_title").html(result);
    $("#landing_description1").html(JSON.stringify(landing_desc1));

    //SHOWING DATA IN OUR SERVICES SECTION
    $("#service_one_image").attr("src", service_one_image);
    $("#service_one_title").html(service_one_title);
    $("#service_one_desc").html(service_one_desc);

    $("#service_two_image").attr("src", service_two_image);
    $("#service_two_title").html(service_two_title);
    $("#service_two_desc").html(service_two_desc);

    $("#service_three_image").attr("src", service_three_image);
    $("#service_three_title").html(service_three_title);
    $("#service_three_desc").html(service_three_desc);

    $("#service_four_image").attr("src", service_four_image);
    $("#service_four_title").html(service_four_title);
    $("#service_four_desc").html(service_four_desc);

    //SHOWING DATA IN THE ABOUT US SECTION
    $("#about_us_desc").html(about_us_desc);

    //SHOWING DATA IN CAREERS SECTION
    $("#job-title").html(job_title);
    $("#job-desc").html(job_desc);
    $("#skill-1").html(job_skill_1);
    $("#skill-2").html(job_skill_2);
    $("#skill-3").html(job_skill_3);
    $("#skill-4").html(job_skill_4);
    $("#optional-cont").html(optional_cont);
    $("#job-date").html(job_date);
    // $("#img1").attr("src", product_image);
}

//SCROLL TO TOP FUNCTION
$(document).ready(function () {

    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.scroll-top').fadeIn();
        } else {
            $('.scroll-top').fadeOut();
        }
    });

    $('.scroll-top').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 100);
        return false;
    });

});